import configparser
import hashlib

from functions import sum, multiply, print_upper

def test_sum():
    assert(sum(2, 2)==4)
    assert(sum(0, 0)==0)
    assert(sum(2,-2)==0)

def test_multiply():
    assert(multiply(2, 2)==4)
    assert(multiply(0, 3)==0)
    assert(multiply(-1, 3)==-3)

def test_always_fail():
    assert(multiply(2, 2)==5)

def test_read_credentials():
    config = configparser.ConfigParser()
    config.read('config.ini')
    password = config['credentials']['password']
    username = config['credentials']['username']
    assert(hashlib.sha256(password.encode('utf-8')).hexdigest()=='1f489582f7ea4c208b70219a2bb6a322227a7516630530a10ed7f2710cfbe447')
    assert(hashlib.sha256(username.encode('utf-8')).hexdigest()=='6b670444e11ba0094fdbca25c145a28a2f8214035cc5d3967cc96e3b02ff3dcd')
